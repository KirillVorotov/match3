﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    public interface IGame
    {
        event Action<List<GameTile>> TilesSpawned;
        event Action<GameTile> TileSpawnedOnField;
        event Action<List<GameTile>> TilesRemoved;
        event Action<GameTile> TileDataChanged;
        GravityDirection Gravity { get; }
        GameField GameField { get; }
        bool Resolved { get; }
        int Moves { get; }
        int MovesLimit { get; }
        int MovesLeft { get; }
        int Score { get; }
        bool AllowSpawnBonuses { get; set; }
        void EnqueueCommand(IGameCommand cmd);
        void Update();
    }
}