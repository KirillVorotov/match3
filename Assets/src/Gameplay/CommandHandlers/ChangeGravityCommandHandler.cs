﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    public class ChangeGravityCommandHandler : GameCommandHandler<Match3Game, ChangeGravityCommand>
    {
        protected override void ProcessGameCommandInternal(Match3Game game, ChangeGravityCommand command)
        {
            switch(game.MovementSystem.Gravity)
            {
                case GravityDirection.Downward:
                    game.MovementSystem.Gravity = GravityDirection.Upward;
                    break;
                case GravityDirection.Upward:
                    game.MovementSystem.Gravity = GravityDirection.Downward;
                    break;
            }
        }
    }
}
