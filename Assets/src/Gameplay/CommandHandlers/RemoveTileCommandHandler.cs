﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    public class RemoveTileCommandHandler : GameCommandHandler<Match3Game, RemoveTileCommand>
    {
        protected override void ProcessGameCommandInternal(Match3Game game, RemoveTileCommand command)
        {
            game.RemoveTile(command.Position);
            game.Resolved = false;
        }
    }
}
