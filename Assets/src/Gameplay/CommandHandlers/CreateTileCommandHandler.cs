﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    public class CreateTileCommandHandler : GameCommandHandler<Match3Game, CreateTileCommand>
    {
        protected override void ProcessGameCommandInternal(Match3Game game, CreateTileCommand command)
        {
            game.SpawnTileOnField(command.Position, command.Data);
            game.Resolved = false;
        }
    }
}
