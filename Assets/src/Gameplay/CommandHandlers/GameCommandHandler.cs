﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace match3.Gameplay
{
    public interface IGameCommandHandler<TGame>
        where TGame : class
    {
        Type GetCommandType();
        void ProcessGameCommand(TGame game, IGameCommand command);
    }

    public abstract class GameCommandHandler<TGame, TCommand> : IGameCommandHandler<TGame> 
        where TGame : class 
        where TCommand : IGameCommand
    {
        public Type GetCommandType()
        {
            return typeof(TCommand);
        }

        public void ProcessGameCommand(TGame game, IGameCommand command)
        {
            if (command is TCommand)
            {
                ProcessGameCommandInternal(game, (TCommand)command);
            }
            else
            {
                Debug.LogErrorFormat("Game command handler {0} cannot process command with type {1}", this.GetType(), command.GetType());
            }
        }

        protected virtual void ProcessGameCommandInternal(TGame game, TCommand command)
        {
            throw new NotImplementedException();
        }
    }
}
