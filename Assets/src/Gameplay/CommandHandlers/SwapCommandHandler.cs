﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace match3.Gameplay
{
    public class SwapCommandHandler : GameCommandHandler<Match3Game, SwapCommand>
    {
        private List<GameTile> matches = new List<GameTile>();

        protected override void ProcessGameCommandInternal(Match3Game game, SwapCommand command)
        {
            if (game.MovesLeft <= 0)
            {
                return;
            }

            if ((command.Position1 - command.Position0).ManhattanLength > 1)
            {
                return;
            }

            var tile0 = game.GameField.GetTile(command.Position0);
            var tile1 = game.GameField.GetTile(command.Position1);

            if (tile0 == null || tile1 == null)
            {
                return;
            }
            if (!tile0.Data.CanMove || !tile1.Data.CanMove)
            {
                return;
            }
            if (tile0.Data.Type == tile1.Data.Type)
            {
                // Do not need to swap identical tiles.
                return;
            }

            game.GameField.SwapTiles(command.Position0, command.Position1);

            int tile0HorizontalMatches = 0;
            int tile0VerticalMatches = 0;

            int tile1HorizontalMatches = 0;
            int tile1VerticalMatches = 0;

            var match = CheckTile(game.GameField, command.Position0, out tile1HorizontalMatches, out tile1VerticalMatches) || 
                CheckTile(game.GameField, command.Position1, out tile0HorizontalMatches, out tile0VerticalMatches);

            if (match)
            {
                game.Moves++;
                game.Resolved = false;
                return;
            }

            // Swap tiles back, restore game field.
            game.GameField.SwapTiles(command.Position0, command.Position1);
        }

        private bool CheckTile(GameField gameField, Point2 point, out int horizontal, out int vertical)
        {
            var tile = gameField.GetTile(point);
            horizontal = 0;
            vertical = 0;

            if (tile == null)
            {
                return false;
            }

            GameTile testTile;
            int x = 1;

            testTile = gameField.GetTile(point + new Point2(x, 0));
            while (testTile != null && testTile.Data.Type == tile.Data.Type)
            {
                x++;
                horizontal++;
                testTile = gameField.GetTile(point + new Point2(x, 0));
            }

            x = -1;

            testTile = gameField.GetTile(point + new Point2(x, 0));
            while (testTile != null && testTile.Data.Type == tile.Data.Type)
            {
                x--;
                horizontal++;
                testTile = gameField.GetTile(point + new Point2(x, 0));
            }

            int y = 1;

            testTile = gameField.GetTile(point + new Point2(0, y));
            while (testTile != null && testTile.Data.Type == tile.Data.Type)
            {
                y++;
                vertical++;
                testTile = gameField.GetTile(point + new Point2(0, y));
            }

            y = -1;

            testTile = gameField.GetTile(point + new Point2(0, y));
            while (testTile != null && testTile.Data.Type == tile.Data.Type)
            {
                y--;
                vertical++;
                testTile = gameField.GetTile(point + new Point2(0, y));
            }

            horizontal++;
            vertical++;
            return horizontal >= 3 || vertical >= 3;
        }
    }
}
