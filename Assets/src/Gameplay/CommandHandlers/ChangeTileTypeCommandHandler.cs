﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    public class ChangeTileTypeCommandHandler : GameCommandHandler<Match3Game, ChangeTileTypeCommand>
    {
        protected override void ProcessGameCommandInternal(Match3Game game, ChangeTileTypeCommand command)
        {
            game.ChangeTileData(command.Position, command.Data);
            game.Resolved = false;
        }
    }
}
