﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    public class TileMovementSystem : ITileMovementSystem
    {
        public GravityDirection Gravity { get; set; } = GravityDirection.Downward;

        public bool Move(ref GameField gameField)
        {
            switch(Gravity)
            {
                case GravityDirection.Downward:
                    return MoveDownward(ref gameField);
                case GravityDirection.Upward:
                    return MoveUpward(ref gameField);
                default:
                    return false;
            }
        }

        private bool MoveDownward(ref GameField gameField)
        {
            GameTile tile;
            bool tileMoved = false;
            int steps = 0;
            for (int x = 0; x < gameField.Size.X; x++)
            {
                steps = 0;
                for (int y = 0; y < gameField.Size.Y; y++)
                {
                    tile = gameField[x, y];
                    if (tile == null)
                    {
                        steps++;
                    }
                    else
                    {
                        Point2 from = tile.Position;
                        Point2 to = new Point2(tile.Position.X, tile.Position.Y - steps);
                        if (gameField.MoveTile(from, to))
                        {
                            tileMoved = true;
                        }
                    }
                }
            }
            return tileMoved;
        }

        private bool MoveUpward(ref GameField gameField)
        {
            GameTile tile;
            bool tileMoved = false;
            int steps = 0;
            for (int x = 0; x < gameField.Size.X; x++)
            {
                steps = 0;
                for (int y = gameField.Size.Y - 1; y >= 0; y--)
                {
                    tile = gameField[x, y];
                    if (tile == null)
                    {
                        steps++;
                    }
                    else
                    {
                        Point2 from = tile.Position;
                        Point2 to = new Point2(tile.Position.X, tile.Position.Y + steps);
                        if (gameField.MoveTile(from, to))
                        {
                            tileMoved = true;
                        }
                    }
                }
            }
            return tileMoved;
        }
    }
}
