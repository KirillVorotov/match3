﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zenject;

namespace match3.Gameplay
{
    public class SimpleSpawnSystem : ITileSpawnSystem
    {
        public void Fill(Random rng, ref GameField gameField, HashSet<TileType> tileTypes)
        {
            if (tileTypes.Count == 0)
            {
                return;
            }

            bool spawn = true;

            // TODO: Fill the game field without pre-matches. For now game controller updates game until it's resolved before spawning game objects.

            for (int x = 0; x < gameField.Size.X; x ++)
            {
                for (int y = 0; y < gameField.Size.Y; y++)
                {
                    if (spawn)
                    {
                        var randomTileType = tileTypes.ElementAt((rng.Next(0, tileTypes.Count)));
                        gameField.CreateTile(new Point2(x, y), new TileData() { CanMove = true, Special = SpecialTileType.Standard, Type = randomTileType, Score = 100 });
                    }
                }
            }
        }

        public bool SpawnNewTiles(Match3Game game, Random rng, ref GameField gameField, HashSet<TileType> tileTypes, out List<GameTile> spawnedTiles)
        {
            switch(game.Gravity)
            {
                case GravityDirection.Downward:
                    return SpawnFromBottomToTop(rng, ref gameField, tileTypes, out spawnedTiles);
                case GravityDirection.Upward:
                    return SpawnFromTopToBottom(rng, ref gameField, tileTypes, out spawnedTiles);
                default:
                    return SpawnFromBottomToTop(rng, ref gameField, tileTypes, out spawnedTiles);
            }
        }

        private bool SpawnFromBottomToTop(Random rng, ref GameField gameField, HashSet<TileType> tileTypes, out List<GameTile> spawnedTiles)
        {
            GameTile tile;
            Point2 position = new Point2();
            bool spawned = false;
            spawnedTiles = new List<GameTile>();

            for (int x = 0; x < gameField.Size.X; x++)
            {
                for (int y = 0; y < gameField.Size.Y; y++)
                {
                    position.X = x;
                    position.Y = y;
                    tile = gameField.GetTile(x, y);
                    if (tile == null)
                    {
                        var randomTileType = tileTypes.ElementAt((rng.Next(0, tileTypes.Count)));
                        spawnedTiles.Add(gameField.CreateTile(position, new TileData() { CanMove = true, Special = SpecialTileType.Standard, Type = randomTileType, Score = 100 }));
                        spawned = true;
                    }
                }
            }
            return spawned;
        }

        private bool SpawnFromTopToBottom(Random rng, ref GameField gameField, HashSet<TileType> tileTypes, out List<GameTile> spawnedTiles)
        {
            GameTile tile;
            Point2 position = new Point2();
            bool spawned = false;
            spawnedTiles = new List<GameTile>();

            for (int x = 0; x < gameField.Size.X; x++)
            {
                for (int y = gameField.Size.Y - 1; y >= 0; y--)
                {
                    position.X = x;
                    position.Y = y;
                    tile = gameField.GetTile(x, y);
                    if (tile == null)
                    {
                        var randomTileType = tileTypes.ElementAt((rng.Next(0, tileTypes.Count)));
                        spawnedTiles.Add(gameField.CreateTile(position, new TileData() { CanMove = true, Special = SpecialTileType.Standard, Type = randomTileType, Score = 100 }));
                        spawned = true;
                    }
                }
            }
            return spawned;
        }
    }
}
