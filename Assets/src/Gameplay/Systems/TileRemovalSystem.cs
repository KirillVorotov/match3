﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    public class TileRemovalSystem : ITileRemovalSystem
    {
        Random rng = new Random();

        public void MarkNeighbours(Match3Game game, ref GameField gameField)
        {
            for (int x = 0; x < gameField.Size.X; x++)
            {
                for (int y = 0; y < gameField.Size.Y; y++)
                {
                    var tile = gameField[x, y];
                    var lowerTile = gameField[x, y - 1];
                    var leftTile = gameField[x - 1, y];

                    if (tile == null)
                    {
                        continue;
                    }
                    if (lowerTile != null && tile.Data.Type == lowerTile.Data.Type)
                    {
                        tile.LowerNeightbours = lowerTile.LowerNeightbours + 1;
                    }
                    else
                    {
                        tile.LowerNeightbours = 0;
                    }

                    if (leftTile != null && tile.Data.Type == leftTile.Data.Type)
                    {
                        tile.LeftNeighbours = leftTile.LeftNeighbours + 1;
                    }
                    else
                    {
                        tile.LeftNeighbours = 0;
                    }
                }
            }

            for (int x = gameField.Size.X; x >= 0; x--)
            {
                for (int y = gameField.Size.X; y >= 0; y--)
                {
                    var tile = gameField[x, y];
                    var upperTile = gameField[x, y + 1];
                    var rightTile = gameField[x + 1, y];

                    if (tile == null)
                    {
                        continue;
                    }
                    if (upperTile != null && tile.Data.Type == upperTile.Data.Type)
                    {
                        tile.UpperNeighbours = upperTile.UpperNeighbours + 1;
                    }
                    else
                    {
                        tile.UpperNeighbours = 0;
                    }

                    if (rightTile != null && tile.Data.Type == rightTile.Data.Type)
                    {
                        tile.RightNeighbours = rightTile.RightNeighbours + 1;
                    }
                    else
                    {
                        tile.RightNeighbours = 0;
                    }
                }
            }
        }

        public bool Remove(Match3Game game, Random rng, ref GameField gameField, out List<GameTile> removedTiles, bool spawnBonuses)
        {
            if (spawnBonuses)
            {
                SpawnBonuses(rng, game, ref gameField);
            }

            removedTiles = new List<GameTile>();
            int changeGravity = 0;

            for (int x = 0; x < gameField.Size.X; x++)
            {
                for (int y = 0; y < gameField.Size.Y; y++)
                {
                    var tile = gameField[x, y];

                    if (tile == null)
                    {
                        continue;
                    }

                    if (tile.UpperNeighbours + tile.LowerNeightbours > 1 || tile.LeftNeighbours + tile.RightNeighbours > 1)
                    {
                        if (tile.Data.Special == SpecialTileType.GravitationSwap)
                        {
                            game.Score += tile.Data.Score * 2;
                            changeGravity++;
                        }
                        else
                        {
                            game.Score += tile.Data.Score;
                        }
                        removedTiles.Add(tile);
                        gameField.RemoveTile(tile.Position.X, tile.Position.Y);
                    }
                }
            }

            if (changeGravity % 2 != 0)
            {
                game.EnqueueCommand(new ChangeGravityCommand());
            }

            return removedTiles.Count > 0;
        }

        private void SpawnBonuses(Random rng, Match3Game game, ref GameField gameField)
        {
            bool justSpawned = false;
            int lineLength = 0;
            int crosses = 0;

            for (int x = 0; x < gameField.Size.X; x++)
            {
                for (int y = 0; y < gameField.Size.Y; y++)
                {
                    var tile = gameField[x, y];

                    if (tile == null)
                    {
                        lineLength = 1;
                        crosses = 0;
                        justSpawned = false;
                        continue;
                    }

                    if ((tile.UpperNeighbours + tile.LowerNeightbours) > 1)
                    {
                        lineLength = tile.UpperNeighbours + tile.LowerNeightbours + 1;
                        if (tile.UpperNeighbours + tile.LowerNeightbours > 1 && tile.LeftNeighbours + tile.RightNeighbours > 1)
                        {
                            crosses++;
                        }

                        if (tile.UpperNeighbours == 0 && (lineLength > 3 || crosses > 0))
                        {
                            if (!justSpawned)
                            {
                                justSpawned = true;
                                game.EnqueueCommand(new CreateTileCommand()
                                {
                                    Position = tile.Position - new Point2(0, rng.Next(0, lineLength)),
                                    Data = new TileData()
                                    {
                                        Type = tile.Data.Type,
                                        Special = SpecialTileType.GravitationSwap,
                                        CanMove = true,
                                        Score = tile.Data.Score
                                    }
                                });
                            }
                        }
                    }
                    else
                    {
                        crosses = 0;
                        justSpawned = false;
                    }
                }
            }

            justSpawned = false;
            lineLength = 0;
            crosses = 0;

            for (int y = 0; y < gameField.Size.Y; y++)
            {
                for (int x = 0; x < gameField.Size.X; x++)
                {
                    var tile = gameField[x, y];

                    if (tile == null)
                    {
                        lineLength = 1;
                        crosses = 0;
                        justSpawned = false;
                        continue;
                    }

                    if ((tile.LeftNeighbours + tile.RightNeighbours) > 1)
                    {
                        lineLength = tile.LeftNeighbours + tile.RightNeighbours + 1;
                        if (tile.UpperNeighbours + tile.LowerNeightbours > 1 && tile.LeftNeighbours + tile.RightNeighbours > 1)
                        {
                            crosses++;
                        }

                        if (tile.RightNeighbours == 0 && (lineLength > 3 || crosses > 0))
                        {
                            if (!justSpawned)
                            {
                                justSpawned = true;
                                game.EnqueueCommand(new CreateTileCommand()
                                {
                                    Position = tile.Position - new Point2(rng.Next(0, lineLength), 0),
                                    Data = new TileData()
                                    {
                                        Type = tile.Data.Type,
                                        Special = SpecialTileType.GravitationSwap,
                                        CanMove = true,
                                        Score = tile.Data.Score
                                    }
                                });
                            }
                        }
                    }
                    else
                    {
                        crosses = 0;
                        justSpawned = false;
                    }
                }
            }
        }
    }
}
