﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    public interface ITileSpawnSystem
    {
        void Fill(Random rng, ref GameField gameField, HashSet<TileType> tileTypes);
        bool SpawnNewTiles(Match3Game game, Random rng, ref GameField gameField, HashSet<TileType> tileTypes, out List<GameTile> spawnedTiles);
    }

    public interface ITileMovementSystem
    {
        GravityDirection Gravity { get; set; }
        bool Move(ref GameField gameField);
    }

    public interface ITileRemovalSystem
    {
        void MarkNeighbours(Match3Game game, ref GameField gameField);
        bool Remove(Match3Game game, Random rng, ref GameField gameField, out List<GameTile> removedTiles, bool spawnBonuses = true);
    }
}
