﻿using System;
using UnityEngine;

namespace match3
{
    [Serializable]
    public struct Point2 : IEquatable<Point2>
    {
        public int X, Y;

        public Point2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int this[int index]
        {
            get
            {
                if (index == 0) return X;
                else if (index == 1) return Y;
                throw new IndexOutOfRangeException("You tried to access this vector at index: " + index);
            }
            set
            {
                if (index == 0) X = value;
                else if (index == 1) Y = value;
                else throw new IndexOutOfRangeException("You tried to set this vector at index: " + index);
            }
        }

        public int ManhattanLength
        {
            get
            {
                return Math.Abs(X) + Math.Abs(Y);
            }
        }

        public bool Equals(Point2 other)
        {
            return this.X == other.X && this.Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Point2))
            {
                return false;
            }
            return Equals((Point2)obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("({0}{2} {1})", X, Y, System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator);
        }

        public static Point2 operator +(Point2 lhs, Point2 rhs)
        {
            lhs.X += rhs.X;
            lhs.Y += rhs.Y;
            return lhs;
        }

        public static Point2 operator -(Point2 lhs, Point2 rhs)
        {
            lhs.X -= rhs.X;
            lhs.Y -= rhs.Y;
            return lhs;
        }

        public static Point2 operator -(Point2 point)
        {
            point.X = -point.X;
            point.Y = -point.Y;
            return point;
        }

        public static Point2 operator *(Point2 lhs, Point2 rhs)
        {
            lhs.X *= rhs.X;
            lhs.Y *= rhs.Y;
            return lhs;
        }

        public static Point2 operator *(Point2 lhs, int rhs)
        {
            lhs.X *= rhs;
            lhs.Y *= rhs;
            return lhs;
        }

        public static bool operator ==(Point2 lhs, Point2 rhs)
        {
            return lhs.Equals(rhs);
        }

        public static bool operator !=(Point2 lhs, Point2 rhs)
        {
            return !lhs.Equals(rhs);
        }

        public static implicit operator Vector2(Point2 value)
        {
            return new Vector2(value.X, value.Y);
        }

        public static implicit operator Vector3(Point2 value)
        {
            return new Vector3(value.X, value.Y, 0);
        }
    }
}
