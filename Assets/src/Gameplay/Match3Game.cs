﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace match3.Gameplay
{
    public class Match3Game : IGame
    {
        public event Action<List<GameTile>> TilesSpawned;
        public event Action<GameTile> TileSpawnedOnField;
        public event Action<List<GameTile>> TilesRemoved;
        public event Action<GameTile> TileDataChanged;
        public GravityDirection Gravity
        {
            get
            {
                return MovementSystem.Gravity;
            }
        }
        public GameField GameField
        {
            get
            {
                return gameField;
            }
        }

        public bool Resolved { get; set; }
        public int Moves { get; set; }
        public int MovesLimit { get; protected set; }
        public int MovesLeft
        {
            get
            {
                return MovesLimit - Moves;
            }
        }
        public bool AllowSpawnBonuses { get; set; }
        public int Score { get; set; }
        public ITileSpawnSystem SpawnSystem { get; protected set; }
        public ITileMovementSystem MovementSystem { get; protected set; }
        public ITileRemovalSystem RemovalSystem { get; protected set; }

        protected Queue<IGameCommand> commandQueue = new Queue<IGameCommand>();
        protected Dictionary<Type, List<IGameCommandHandler<Match3Game>>> commandHandlers = new Dictionary<Type, List<IGameCommandHandler<Match3Game>>>();
        protected GameField gameField;
        protected int randomSeed;
        protected System.Random rng;
        protected HashSet<TileType> avaiableTileTypes = new HashSet<TileType>();

        public Match3Game(GameConfig config, ITileSpawnSystem spawnSystem, ITileMovementSystem movementSystem, ITileRemovalSystem removalSystem, params IGameCommandHandler<Match3Game>[] commandHandlers)
        {
            SpawnSystem = spawnSystem;
            this.MovementSystem = movementSystem;
            this.RemovalSystem = removalSystem;
            this.randomSeed = config.RandomSeed;
            rng = new System.Random(config.RandomSeed);
            if (commandHandlers != null)
            {
                for (int i = 0; i < commandHandlers.Length; i++)
                {
                    if (!this.commandHandlers.ContainsKey(commandHandlers[i].GetCommandType()))
                    {
                        this.commandHandlers.Add(commandHandlers[i].GetCommandType(), new List<IGameCommandHandler<Match3Game>>());
                    }
                    this.commandHandlers[commandHandlers[i].GetCommandType()].Add(commandHandlers[i]);
                    Debug.LogFormat("{0}: Added a game command handler {1} for the command type {2}", GetType(), commandHandlers[i].GetType(), commandHandlers[i].GetCommandType());
                }
            }

            gameField = new GameField(config.FieldSize);
            avaiableTileTypes = config.AvailableTiles;
            spawnSystem.Fill(rng, ref gameField, avaiableTileTypes);
            Resolved = false;
            MovesLimit = (config.MovesLimit > 0) ? config.MovesLimit : 0;
            Moves = 0;

            AllowSpawnBonuses = false;
            while(!Resolved)
            {
                Update();
            }
            AllowSpawnBonuses = true;
            Score = 0;
            Moves = 0;
        }

        public void EnqueueCommand(IGameCommand cmd)
        {
            commandQueue.Enqueue(cmd);
            Resolved = false;
        }

        public void Update()
        {
            Resolved = true;

            if (commandQueue.Count > 0)
            {
                Resolved = false;
                var cmd = commandQueue.Dequeue();
                if (cmd != null)
                {
                    ProcessGameCommand(cmd);
                }
                Resolved = false;
                return;
            }

            if (MovementSystem.Move(ref gameField))
            {
                Resolved = false;
                //return; // comment if you want to drop tiles and spawn new ones at the same time.
            }

            List<GameTile> spawnedTiles;
            if (SpawnSystem.SpawnNewTiles(this, rng, ref gameField, avaiableTileTypes, out spawnedTiles))
            {
                TilesSpawned?.Invoke(spawnedTiles);
                Resolved = false;
                return;
            }

            RemovalSystem.MarkNeighbours(this, ref gameField);
            List<GameTile> removedTiles;
            if (RemovalSystem.Remove(this, rng, ref gameField, out removedTiles, AllowSpawnBonuses))
            {
                TilesRemoved?.Invoke(removedTiles);
                Resolved = false;
                return;
            }

            // TODO: Check if there are no possible moves - regenerate field.
            //if (!MatchingSystem.PossibleMoves(ref gameField))
            //{
            //    gameField.Clear();
            //    Resolved = false;
            //    return;
            //}
        }

        public void SpawnTileOnField(Point2 position, TileData data)
        {
            var tile = gameField.CreateTile(position, data);
            if (tile != null && TileSpawnedOnField != null)
            {
                TileSpawnedOnField(tile);
            }
        }

        public void RemoveTile(Point2 position)
        {
            var tile = GameField.GetTile(position);
            if (tile == null)
            {
                return;
            }
            GameField.RemoveTile(position);
            TilesRemoved?.Invoke(new List<GameTile>() { tile });
        }

        public void ChangeTileData(Point2 position, TileData data)
        {
            var tile = GameField.GetTile(position);
            if (tile != null)
            {
                tile.Data = data;
                TileDataChanged?.Invoke(tile);
            }
        }

        protected void ProcessGameCommand(IGameCommand cmd)
        {
            Type cmdType = cmd.GetType();
            if (commandHandlers.ContainsKey(cmdType))
            {
                foreach (var handler in commandHandlers[cmdType])
                {
                    handler.ProcessGameCommand(this, cmd);
                }
            }
        }
    }
}
