﻿using System;

namespace match3.Gameplay
{
    public interface IGameCommand
    {
    }

    public struct SwapCommand : IGameCommand
    {
        public Point2 Position0;
        public Point2 Position1;
    }

    public struct ChangeTileTypeCommand : IGameCommand
    {
        public Point2 Position;
        public TileData Data;
    }
    public struct CreateTileCommand : IGameCommand
    {
        public Point2 Position;
        public TileData Data;
    }

    public struct RemoveTileCommand : IGameCommand
    {
        public Point2 Position;
    }

    public struct ChangeGravityCommand : IGameCommand
    {
    }

    public struct ClearFieldCommand : IGameCommand
    {
    }
}
