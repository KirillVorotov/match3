﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    [Serializable]
    public class GameConfig
    {
        public int RandomSeed;
        public Point2 FieldSize;
        public HashSet<TileType> AvailableTiles;
        public int MovesLimit;
    }
}
