﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    [Serializable]
    public class GameField
    {
        public Point2 Size
        {
            get
            {
                return new Point2(tiles.GetLength(0), tiles.GetLength(1));
            }
        }

        private GameTile[,] tiles;
        private uint nextAvaliableID = 0;

        public GameField(Point2 size)
        {
            if (size.X <= 0 || size.Y <= 0)
            {
                tiles = new GameTile[0, 0];
            }
            else
            {
                tiles = new GameTile[size.X, size.Y];
            }
        }

        public GameTile this[int x, int y]
        {
            get
            {
                if (x < 0 || y < 0 || x >= Size.X || y >= Size.Y)
                {
                    return null;
                }
                return tiles[x, y];
            }
            set
            {
                if (x < 0 || y < 0 || x >= Size.X || y >= Size.Y)
                {
                    return;
                }
                tiles[x, y] = value;
            }
        }

        public GameTile CreateTile(Point2 position, TileData data)
        {
            if (position.X < 0 || position.X >= Size.X || position.Y < 0 || position.Y >= Size.Y)
            {
                return null;
            }
            if (tiles[position.X, position.Y] == null)
            {
                tiles[position.X, position.Y] = new GameTile() { ID = nextAvaliableID, Data = data, Position = position };
                nextAvaliableID++;
                return tiles[position.X, position.Y];
            }
            return null;
        }

        public void RemoveTile(int x, int y)
        {
            if (x < 0 || x >= Size.X || y < 0 || y >= Size.Y)
            {
                return;
            }
            if (tiles[x, y] != null)
            {
                tiles[x, y] = null;
            }
        }

        public void RemoveTile(Point2 position)
        {
            RemoveTile(position.X, position.Y);
        }

        public GameTile GetTile(Point2 position)
        {
            if (position.X < 0 || position.X >= Size.X || position.Y < 0 || position.Y >= Size.Y)
            {
                return null;
            }
            return tiles[position.X, position.Y];
        }

        public GameTile GetTile(int x, int y)
        {
            if (x < 0 || x >= Size.X || y < 0 || y >= Size.Y)
            {
                return null;
            }
            return tiles[x, y];
        }

        public void SwapTiles(Point2 position0, Point2 position1)
        {
            if (position0.X < 0 || position0.X >= Size.X || position0.Y < 0 || position0.Y >= Size.Y)
            {
                return;
            }
            if (position1.X < 0 || position1.X >= Size.X || position1.Y < 0 || position1.Y >= Size.Y)
            {
                return;
            }
            var tile0 = GetTile(position0);
            var tile1 = GetTile(position1);
            if (tile1 != null)
            {
                SetTile(position0, tile1);
            }
            if (tile0 != null)
            {
                SetTile(position1, tile0);
            }
        }

        public bool MoveTile(Point2 from, Point2 to, bool replace = false)
        {
            if (from.X < 0 || from.X >= Size.X || from.Y < 0 || from.Y >= Size.Y)
            {
                return false;
            }
            if (from.X < 0 || from.X >= Size.X || from.Y < 0 || from.Y >= Size.Y)
            {
                return false;
            }
            var sourceTile = GetTile(from);
            var destTile = GetTile(to);
            bool canMove = (destTile != null && replace) || (destTile == null);
            if (canMove)
            {
                if (destTile != null)
                {
                    RemoveTile(destTile.Position);
                }
                sourceTile.Position = to;
                tiles[from.X, from.Y] = null;
                tiles[to.X, to.Y] = sourceTile;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Clear()
        {
            for (int x = 0; x < Size.X; x++)
            {
                for (int y = 0; y < Size.Y; y++)
                {
                    RemoveTile(x, y);
                }
            }
        }

        private void SetTile(Point2 position, GameTile tile)
        {
            if (position.X < 0 || position.X >= Size.X || position.Y < 0 || position.Y >= Size.Y)
            {
                return;
            }
            if (tiles[position.X, position.Y] != null)
            {
                tiles[position.X, position.Y] = tile;
                tiles[position.X, position.Y].Position = position;
            }
        }
    }
}
