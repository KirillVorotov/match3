﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    [Serializable]
    public class GameTile
    {
        public uint ID;
        public Point2 Position;
        public TileData Data;

        public int HorizontalMatches = 0;
        public int VerticalMatches = 0;

        public int UpperNeighbours = 0;
        public int LowerNeightbours = 0;
        public int LeftNeighbours = 0;
        public int RightNeighbours = 0;
    }
}
