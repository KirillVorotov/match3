﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    [Serializable]
    public struct TileData
    {
        public TileType Type;
        public SpecialTileType Special;
        public bool CanMove;
        public int Score;
    }
}
