﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace match3.Gameplay
{
    [Serializable]
    public enum TileType : int
    {
        Elephant,
        Giraffe,
        Hippo,
        Monkey,
        Panda,
        Parrot,
        Penguin,
        Pig,
        Rabbit,
        Snake,
    }
}
