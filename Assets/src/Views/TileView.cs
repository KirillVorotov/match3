﻿using match3.Controllers;
using match3.Gameplay;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace match3.Views
{
    public class TileView : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField]
        private SpriteRenderer graphics, selection, special;
        [SerializeField]
        private float movementSpeed = 5f;
        [SerializeField]
        private Animator animator;
        [SerializeField]
        public Sprite ElephantSprite, GiraffeSprite, HippoSprite, MonkeySprite, PandaSprite, ParrotSprite, PenguinSprite, PigSprite, RabbitSprite, SnakeSprite;
        public bool Removed = false;

        private GameController gameController;
        private Vector3 targetPosition;

        public GameTile Tile;
        public Transform CachedTransform { get; private set; }
        public bool Busy { get; protected set; } = false;
        public Vector3 WorldPosition
        {
            get
            {
                return CachedTransform.position;
            }
            set
            {
                CachedTransform.position = value;
            }
        }

        public Vector3 LocalPosition
        {
            get
            {
                return CachedTransform.localPosition;
            }
            set
            {
                CachedTransform.localPosition = value;
            }
        }

        public SpriteRenderer Graphics
        {
            get
            {
                return graphics;
            }
        }

        public SpriteRenderer Selection
        {
            get
            {
                return selection;
            }
        }

        public Animator Animator
        {
            get
            {
                return animator;
            }
        }

        [Inject]
        public void Initialize(GameController gameController)
        {
            CachedTransform = GetComponent<Transform>();
            this.gameController = gameController;
        }

        public void UpdateView(float t, float dt)
        {
            //if (Tile.Removed)
            if (Removed)
            {
                animator.SetTrigger("Removal");
                if (animator.GetCurrentAnimatorStateInfo(0).IsName("TileView_Removed"))
                {
                    Busy = false;
                }
                else
                {
                    Busy = true;
                }
            }
            else
            {
                if (!Tile.Data.CanMove)
                {
                    return;
                }
                var localPosition = CachedTransform.localPosition;
                targetPosition = gameController.GameFieldToWorldPosition(Tile.Position);
                var distance = (targetPosition - localPosition).sqrMagnitude;
                Busy = distance > 0.0001f;
                if (Busy)
                {
                    var movementDirection = (targetPosition - localPosition).normalized * movementSpeed * dt;
                    if (movementDirection.sqrMagnitude > distance)
                    {
                        localPosition = targetPosition;
                    }
                    else
                    {
                        localPosition += movementDirection;
                    }
                    CachedTransform.localPosition = localPosition;
                }
            }
            switch(Tile.Data.Special)
            {
                case SpecialTileType.Standard:
                    special.enabled = false;
                    break;
                case SpecialTileType.GravitationSwap:
                    special.enabled = true;
                    break;
            }
        }

        public void SetSelectionVisible(bool value)
        {
            Selection.enabled = value;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            gameController.SelectTile(Tile.ID);
            //gameController.RemoveTile(Tile.ID);
        }

        private void OnDestroy()
        {
            
        }
    }
}
