﻿using match3.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using TMPro;
using Zenject;

namespace match3.Views
{
    public class GameStatisticsView : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI score, moves;
        private GameController gameController;

        [Inject]
        public void Initialize(GameController gameController)
        {
            this.gameController = gameController;
        }

        public void UpdateView(int score, int moves)
        {
            this.score.text = score.ToString();
            this.moves.text = moves.ToString();
        }
    }
}
