﻿using match3.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace match3.Views
{
    public class LevelSelectionButtonView : MonoBehaviour
    {
        public uint LevelID;
        public Button button;

        private StartScreenController startScreenController;

        [Inject]
        public void Initialize(StartScreenController startScreenController)
        {
            this.startScreenController = startScreenController;
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            startScreenController.StartLevel(LevelID);
        }
    }
}