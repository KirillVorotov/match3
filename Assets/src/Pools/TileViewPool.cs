﻿using match3.Gameplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace match3.Views
{
    public class TileViewPool : MonoMemoryPool<GameTile, Transform, Vector3, TileView>
    {
        private Transform poolRoot;

        protected override void OnCreated(TileView item)
        {
            poolRoot = item.CachedTransform.parent;
            item.SetSelectionVisible(false);
            item.gameObject.SetActive(false);
            item.Graphics.enabled = true;
            base.OnCreated(item);
        }

        protected override void Reinitialize(GameTile tile, Transform parent, Vector3 localPosition, TileView item)
        {
            item.CachedTransform.SetParent(parent);
            item.LocalPosition = localPosition;
            item.Tile = tile;
            item.Selection.enabled = false;
            item.Removed = false;
            // TODO: need a sprite collection injected into the TileView to make it easier to add graphics to the new tiles if needed.
            switch (tile.Data.Type)
            {
                case TileType.Elephant:
                    item.Graphics.sprite = item.ElephantSprite;
                    break;
                case TileType.Giraffe:
                    item.Graphics.sprite = item.GiraffeSprite;
                    break;
                case TileType.Hippo:
                    item.Graphics.sprite = item.HippoSprite;
                    break;
                case TileType.Monkey:
                    item.Graphics.sprite = item.MonkeySprite;
                    break;
                case TileType.Panda:
                    item.Graphics.sprite = item.PandaSprite;
                    break;
                case TileType.Parrot:
                    item.Graphics.sprite = item.ParrotSprite;
                    break;
                case TileType.Penguin:
                    item.Graphics.sprite = item.PenguinSprite;
                    break;
                case TileType.Pig:
                    item.Graphics.sprite = item.PigSprite;
                    break;
                case TileType.Rabbit:
                    item.Graphics.sprite = item.RabbitSprite;
                    break;
                case TileType.Snake:
                    item.Graphics.sprite = item.SnakeSprite;
                    break;
            }
            base.Reinitialize(tile, parent, localPosition, item);
        }

        protected override void OnDespawned(TileView item)
        {
            if (item.CachedTransform.parent != poolRoot)
            {
                item.CachedTransform.SetParent(poolRoot);
            }
            item.Tile = null;
            item.Animator.Play("Idle");
            item.SetSelectionVisible(false);
            item.gameObject.SetActive(false);
            item.Graphics.enabled = true;
            base.OnDespawned(item);
        }
    }
}
