﻿using match3.Gameplay;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace match3
{
    public class GameApplication
    {
        public event Action<uint> StartLevelEvent;
        public event Action QuitLevelEvent;

        public GameApplication()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;
            SceneManager.LoadSceneAsync("Game", LoadSceneMode.Additive);
        }

        public void StartLevel(uint levelID)
        {
            if (StartLevelEvent != null)
            {
                StartLevelEvent(levelID);
            }
        }

        public void QuitLevel()
        {
            if (QuitLevelEvent != null)
            {
                QuitLevelEvent();
            }
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Debug.LogFormat("Scene {0} loaded.", scene.name);
        }

        private void OnSceneUnloaded(Scene scene)
        {
            Debug.LogFormat("Scene {0} unloaded.", scene.name);
        }
    }
}
