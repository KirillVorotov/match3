﻿using UnityEngine.EventSystems;
using Zenject;

namespace match3.Installers
{
    public class EventSystemInstaller : MonoInstaller
    {
        public EventSystem eventSystem;
        public override void InstallBindings()
        {
            Container.Bind<EventSystem>().FromComponentInNewPrefab(eventSystem).AsSingle().NonLazy();
        }
    }
}
