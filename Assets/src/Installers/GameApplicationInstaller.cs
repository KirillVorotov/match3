﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zenject;

namespace match3.Installers
{
    public class GameApplicationInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<GameApplication>().AsSingle().NonLazy();
        }
    }
}
