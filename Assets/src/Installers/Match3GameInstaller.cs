﻿using match3.Gameplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zenject;

namespace match3.Installers
{
    public class Match3GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindFactory<GameConfig , IGame, GameFactory>().To<Match3Game>();

            // Command handlers.
            Container.Bind<IGameCommandHandler<Match3Game>>().To<SwapCommandHandler>().AsTransient();
            Container.Bind<IGameCommandHandler<Match3Game>>().To<ClearFieldCommandHandler>().AsTransient();
            Container.Bind<IGameCommandHandler<Match3Game>>().To<RemoveTileCommandHandler>().AsTransient();
            Container.Bind<IGameCommandHandler<Match3Game>>().To<ChangeGravityCommandHandler>().AsTransient();
            Container.Bind<IGameCommandHandler<Match3Game>>().To<CreateTileCommandHandler>().AsTransient();
            Container.Bind<IGameCommandHandler<Match3Game>>().To<ChangeTileTypeCommandHandler>().AsTransient();

            // Game systems.
            Container.Bind<ITileSpawnSystem>().To<SimpleSpawnSystem>().AsSingle();
            Container.Bind<ITileMovementSystem>().To<TileMovementSystem>().AsTransient();
            Container.Bind<ITileRemovalSystem>().To<TileRemovalSystem>().AsTransient();
        }
    }
}
