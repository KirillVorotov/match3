﻿using Rewired;
using Zenject;

namespace match3.Installers
{
    public class RewiredInstaller : MonoInstaller
    {
        public InputManager inputManagerPrefab;
        public override void InstallBindings()
        {
            Container.Bind<InputManager>().FromComponentInNewPrefab(inputManagerPrefab).AsSingle().NonLazy();
        }
    }
}
