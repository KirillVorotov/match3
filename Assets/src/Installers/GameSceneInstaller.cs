﻿using match3.Gameplay;
using match3.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace match3.Installers
{
    public class GameSceneInstaller : MonoInstaller
    {
        [Serializable]
        public class Settings
        {
            public TileView tileViewPrefab;
        }

        public Settings settings;

        public override void InstallBindings()
        {
            Container.BindMemoryPool<TileView, TileViewPool>().WithInitialSize(128).FromComponentInNewPrefab(settings.tileViewPrefab).UnderTransformGroup("TileViewPool");
        }
    }
}
