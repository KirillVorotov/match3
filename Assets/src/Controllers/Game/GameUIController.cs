﻿using match3.Gameplay;
using match3.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace match3.Controllers
{
    public class GameUIController : MonoBehaviour
    {
        [SerializeField]
        private Button menuButton, quitButton, resumeButton, endGameQuitButton;
        [SerializeField]
        private RectTransform ingameMenu, endGameMenu;
        [SerializeField]
        private GameStatisticsView gameStatisticsView;
        GameController gameController;

        [Inject]
        public void Initialize(GameController gameController)
        {
            this.gameController = gameController;

            gameController.GameEnded += OnGameEnded;

            menuButton.onClick.RemoveAllListeners();
            quitButton.onClick.RemoveAllListeners();
            resumeButton.onClick.RemoveAllListeners();
            endGameQuitButton.onClick.RemoveAllListeners();

            menuButton.onClick.AddListener(OnMenuButtonClick);
            quitButton.onClick.AddListener(OnQuitButtonClick);
            resumeButton.onClick.AddListener(OnResumeButtonClick);
            endGameQuitButton.onClick.AddListener(OnQuitButtonClick);

            ShowEndGameMenu(false);
            ShowIngameMenu(false);
        }

        private void OnGameEnded(GameResult result)
        {
            ShowEndGameMenu(true);
            ShowIngameMenu(false);
        }

        private void OnMenuButtonClick()
        {
            if (!gameController.AllowUserInput)
            {
                return;
            }
            ShowIngameMenu(true);
        }

        private void OnQuitButtonClick()
        {
            ShowEndGameMenu(false);
            ShowIngameMenu(false);
            gameController.QuitGame();
        }

        private void OnResumeButtonClick()
        {
            ShowIngameMenu(false);
        }

        private void ShowIngameMenu(bool value)
        {
            ingameMenu.gameObject.SetActive(value);
        }

        private void ShowEndGameMenu(bool value)
        {
            endGameMenu.gameObject.SetActive(value);
        }
    }
}
