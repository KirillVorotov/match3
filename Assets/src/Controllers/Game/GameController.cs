﻿using match3.Gameplay;
using match3.Views;
using Stateless;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace match3.Controllers
{
    public class GameController : MonoBehaviour
    {
        public event Action<GameResult> GameEnded;
        public bool AllowUserInput { get; protected set; }
        public IGame CurrentGame { get; protected set; }
        public Vector2 GridStep = new Vector2(1, 1);
        public Vector2 GridOffset = new Vector2(0, 0);
        [SerializeField]
        private Transform gameRoot, tilesRoot;
        [SerializeField]
        private GameStatisticsView statisticsView;
        private GameApplication gameApplication;
        private GameFactory gameFactory;
        private TileViewPool tileViewPool;
        private Dictionary<uint, TileView> tiles = new Dictionary<uint, TileView>();
        private TileView selectedTile;
        private bool gameRunning = false;
        private System.Random rng = new System.Random(666);

        // Commands
        private SwapCommand swapCommand = new SwapCommand();

        [Inject]
        public void Initialize(GameApplication gameApplication, GameFactory gameFactory, TileViewPool tileViewPool)
        {
            this.gameApplication = gameApplication;
            this.gameFactory = gameFactory;
            this.tileViewPool = tileViewPool;
            this.gameApplication.StartLevelEvent += OnStartLevelEvent;
            this.gameApplication.QuitLevelEvent += OnQuitLevelEvent;
            gameRoot.gameObject.SetActive(false);
            gameRunning = false;
        }

        public void StartGame()
        {
            Debug.LogFormat("GameController: Start game.");
            GameConfig config = new GameConfig()
            {
                RandomSeed = rng.Next(),
                FieldSize = new Point2(8, 8),
                AvailableTiles = new HashSet<TileType>() { TileType.Elephant, TileType.Giraffe, TileType.Hippo, TileType.Monkey, TileType.Parrot, TileType.Snake },
                MovesLimit = 25,
            };
            CurrentGame = gameFactory.Create(config);
            CurrentGame.TilesSpawned += OnTilesSpawn;
            CurrentGame.TileSpawnedOnField += OnTileSpawnedOnField;
            CurrentGame.TilesRemoved += OnTilesRemoved;
            AllowUserInput = true;
            gameRoot.gameObject.SetActive(true);
            tiles.Clear();
            for (int x = 0; x < CurrentGame.GameField.Size.X; x++)
            {
                for (int y = 0; y < CurrentGame.GameField.Size.Y; y++)
                {
                    var worldPosition = GameFieldToWorldPosition(x, y);
                    var tile = CurrentGame.GameField.GetTile(new Point2(x, y));
                    if (tile != null)
                    {
                        tiles.Add(tile.ID, tileViewPool.Spawn(tile, tilesRoot, worldPosition));
                    }
                }
            }
            selectedTile = null;
            gameRunning = true;
            UpdateTileViews();
        }

        public void QuitGame()
        {
            Debug.LogFormat("GameController: Quit game.");
            foreach (var kv in tiles)
            {
                tileViewPool.Despawn(kv.Value);
            }
            tiles.Clear();
            CurrentGame.TilesSpawned -= OnTilesSpawn;
            CurrentGame.TileSpawnedOnField += OnTileSpawnedOnField;
            CurrentGame.TilesRemoved -= OnTilesRemoved;
            CurrentGame = null;
            gameRoot.gameObject.SetActive(false);
            CurrentGame = null;
            gameApplication.QuitLevel();
        }

        public void SelectTile(uint tileID)
        {
            if (!AllowUserInput)
            {
                return;
            }
            if (!tiles.ContainsKey(tileID))
            {
                Debug.LogErrorFormat("Cannot find tile view with id {0}", tileID);
                ClearSelection();
                return;
            }
            if (!tiles[tileID].Tile.Data.CanMove)
            {
                return;
            }
            if (selectedTile == null)
            {
                selectedTile = tiles[tileID];
                selectedTile.SetSelectionVisible(true);
            }
            else
            {
                if (tileID == selectedTile.Tile.ID)
                {
                    ClearSelection();
                    return;
                }

                if ((tiles[tileID].Tile.Position - selectedTile.Tile.Position).ManhattanLength <= 1) // check if adjacent tiles
                {
                    swapCommand.Position0 = selectedTile.Tile.Position;
                    swapCommand.Position1 = tiles[tileID].Tile.Position;
                    AddGameCommand(swapCommand);
                    ClearSelection();
                }
                else
                {
                    ClearSelection();
                    selectedTile = tiles[tileID];
                    selectedTile.SetSelectionVisible(true);
                }
            }
        }

        public void RemoveTile(uint tileID)
        {
            if (!AllowUserInput)
            {
                return;
            }
            if (!tiles.ContainsKey(tileID))
            {
                Debug.LogErrorFormat("Cannot find tile view with id {0}", tileID);
                return;
            }
            AddGameCommand(new RemoveTileCommand() { Position = tiles[tileID].Tile.Position });
        }

        public void ClearSelection()
        {
            if (selectedTile != null)
            {
                selectedTile.SetSelectionVisible(false);
            }
            selectedTile = null;
        }

        public void AddGameCommand(IGameCommand cmd)
        {
            if (!AllowUserInput)
            {
                return;
            }
            CurrentGame.EnqueueCommand(cmd);
            CurrentGame.Update();
        }

        private void UpdateTileViews()
        {
            List<uint> toDespawn = new List<uint>();
            AllowUserInput = true;
            foreach (var kv in tiles)
            {
                kv.Value.UpdateView(Time.time, Time.deltaTime);
                if (kv.Value.Busy)
                {
                    AllowUserInput = false;
                }
                //else if (kv.Value.Tile.Removed)
                else if (kv.Value.Removed)
                {
                    toDespawn.Add(kv.Key);
                }
            }
            foreach(var key in toDespawn)
            {
                tiles[key].Tile = null;
                tileViewPool.Despawn(tiles[key]);
                tiles.Remove(key);
            }
        }

        public Vector2 GameFieldToWorldPosition(Point2 value)
        {
            return GridOffset + (new Vector2(value.X, value.Y) - (Vector2)CurrentGame.GameField.Size * 0.5f + new Vector2(0.5f, 0.5f)) * GridStep;
        }

        private void OnTilesSpawn(List<GameTile> tiles)
        {
            tiles.Sort((a, b) => { return a.Position.X - b.Position.X; });
            foreach(var tile in tiles)
            {
                var worldPosition = new Vector3();
                switch (CurrentGame.Gravity)
                {
                    case GravityDirection.Downward:
                        worldPosition = GameFieldToWorldPosition(new Point2(tile.Position.X, CurrentGame.GameField.Size.Y));
                        break;
                    case GravityDirection.Upward:
                        worldPosition = GameFieldToWorldPosition(new Point2(tile.Position.X, -1));
                        break;
                }
                this.tiles.Add(tile.ID, tileViewPool.Spawn(tile, tilesRoot, worldPosition));
            }
        }

        private void OnTileSpawnedOnField(GameTile tile)
        {
            var worldPosition = GameFieldToWorldPosition(tile.Position);
            tiles.Add(tile.ID, tileViewPool.Spawn(tile, tilesRoot, worldPosition));
        }

        private void OnTilesRemoved(List<GameTile> tiles)
        {
            foreach(var tile in tiles)
            {
                if (this.tiles.ContainsKey(tile.ID))
                {
                    this.tiles[tile.ID].Removed = true;
                }
            }
        }

        private void Update()
        {
            if (CurrentGame == null)
            {
                return;
            }
            if(!gameRunning)
            {
                return;
            }
            if (AllowUserInput && CurrentGame.Resolved && CurrentGame.MovesLeft <= 0)
            {
                GameEnded?.Invoke(new GameResult() { Result = GameResult.ResultType.Victory });
                gameRunning = false;
                return;
            }
            UpdateTileViews();
            statisticsView.UpdateView(CurrentGame.Score, CurrentGame.MovesLeft);
            if (AllowUserInput)
            {
                if (!CurrentGame.Resolved)
                {
                    CurrentGame.Update();
                    AllowUserInput = false;
                }
            }
        }

        private Vector2 GameFieldToWorldPosition(int x, int y)
        {
            return GridOffset + (new Vector2(x, y) - (Vector2)CurrentGame.GameField.Size * 0.5f + new Vector2(0.5f, 0.5f)) * GridStep;
        }

        private void OnStartLevelEvent(uint levelID)
        {
            // TODO: Here we may want to load different level configurations, but for now I'll live it like this.
            StartGame();
        }

        private void OnQuitLevelEvent()
        {
            
        }

        private void OnDestroy()
        {
            this.gameApplication.StartLevelEvent -= OnStartLevelEvent;
        }
    }
}
