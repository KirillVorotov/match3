﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace match3.Controllers
{
    public class StartScreenController : MonoBehaviour
    {
        [SerializeField]
        private Button startButton;
        [SerializeField]
        private RectTransform startMenuScreen, levelSelectionScreen;
        [SerializeField]
        private Canvas startScreenCanvas;

        private GameApplication gameApplication;

        [Inject]
        public void Initialize(GameApplication gameApplication)
        {
            this.gameApplication = gameApplication;
            gameApplication.StartLevelEvent += OnStartLevelEvent;
            gameApplication.QuitLevelEvent += OnQuitLevelEvent;
            startScreenCanvas.gameObject.SetActive(true);
            startMenuScreen.gameObject.SetActive(true);
            levelSelectionScreen.gameObject.SetActive(false);

            startButton.onClick.RemoveAllListeners();
            startButton.onClick.AddListener(OnStartButtonClick);
        }

        public void StartLevel(uint levelID)
        {
            gameApplication.StartLevel(levelID);
        }

        private void OnStartButtonClick()
        {
            startMenuScreen.gameObject.SetActive(false);
            levelSelectionScreen.gameObject.SetActive(true);
        }

        private void OnStartLevelEvent(uint levelID)
        {
            startScreenCanvas.gameObject.SetActive(false);
            startMenuScreen.gameObject.SetActive(false);
            levelSelectionScreen.gameObject.SetActive(false);
        }

        private void OnQuitLevelEvent()
        {
            startScreenCanvas.gameObject.SetActive(true);
            startMenuScreen.gameObject.SetActive(false);
            levelSelectionScreen.gameObject.SetActive(true);
        }
    }
}
